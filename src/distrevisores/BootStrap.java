/*
 * Classe para inicializar os dados
 * Depois de garantida a persistencia dos dados pode ser apagada
 */
package distrevisores;

import distrevisores.business.Revisor;
import distrevisores.business.Topico;
import distrevisores.business.Artigo;
import distrevisores.business.Evento;
import distrevisores.business.Organizador;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author José Pedro Moreira
 */
public class BootStrap {
    
    //Variáveis de instância
    
    public BootStrap() throws Exception{
    }
    
    //Criação de Datas
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date date1 = sdf.parse("2009-12-30");
    Date date1f = sdf.parse("2010-01-05");
    
    Date date2 = sdf.parse("2010-01-31");
    Date date2f = sdf.parse("2010-02-10");
    

    
    //Criação de Organizadores
    Organizador org1 = new Organizador("Bruno", "Bruno@mail.com");
    Organizador org2 = new Organizador("Manuel", "Manuel@mail.com");
    
    //Criação de Eventos
    Evento ev1 = new Evento ("Evento A", "Cirurgia Geral", date1, date1f, "Porto",1);
    Evento ev2 = new Evento ("Evento B", "Pediatria", date2, date2f, "Lisboa",2);
    
    
    
    //Inserção de artigos
    Artigo a1 = new Artigo("A1");
    Artigo a2 = new Artigo("A2");
    Artigo a3 = new Artigo("A3");
    Artigo a4 = new Artigo("A4");
    Artigo a5 = new Artigo("A5");
    Artigo a6 = new Artigo("A6");
    Artigo a7 = new Artigo("A7");
    Artigo a8 = new Artigo("A8");
    Artigo a9 = new Artigo("A9");
    
    //Criação dos topicos
    Topico t1 = new Topico("topico1","acm1");
    Topico t2 = new Topico("topico2","acm2");
    Topico t3 = new Topico("topico3","acm3");
    Topico t4 = new Topico("topico4","acm4");
    Topico t5 = new Topico("topico5","acm5");
    Topico t6 = new Topico("topico6","acm6");
    Topico t7 = new Topico("topico7","acm7");
    Topico t8 = new Topico("topico8","acm8");
    Topico t9 = new Topico("topico9","acm9");
    Topico t10 = new Topico("topico10","acm10");
    Topico t11 = new Topico("topico11","acm11");
    Topico t12 = new Topico("topico12","acm12");
    Topico t13 = new Topico("topico13","acm13");
    Topico t14 = new Topico("topico14","acm14");
    Topico t15 = new Topico("topico15","acm15");
    Topico t16 = new Topico("topico16","acm16");
    Topico t17 = new Topico("topico17","acm17");
    Topico t18 = new Topico("topico18","acm18");
    Topico t19 = new Topico("topico19","acm19");
    Topico t20 = new Topico("topico20","acm20");
    Topico t21 = new Topico("topico21","acm21");
    Topico t22 = new Topico("topico22","acm22");
    Topico t23 = new Topico("topico23","acm23");
    Topico t24 = new Topico("topico24","acm24");
    Topico t25 = new Topico("topico25","acm25");
    Topico t26 = new Topico("topico26","acm26");
    Topico t27 = new Topico("topico27","acm27");
    
    Revisor r1 = new Revisor("revisor1","r1@ed");
    Revisor r2 = new Revisor("revisor2","r2@ed");
    Revisor r3 = new Revisor("revisor3","r3@ed");
    Revisor r4 = new Revisor("revisor4","r4@ed");
    Revisor r5 = new Revisor("revisor5","r5@ed");
    Revisor r6 = new Revisor("revisor6","r6@ed");
    Revisor r7 = new Revisor("revisor7","r7@ed");
    Revisor r8 = new Revisor("revisor8","r8@ed");
    Revisor r9 = new Revisor("revisor9","r9@ed");
    
    //Atribuição dos tópicos aos artigos
    public void atribuicaoTopicosAosArtigos(){
        a1.addTopico(t1); a1.addTopico(t9); a1.addTopico(t18); a1.addTopico(t1);
        a2.addTopico(t1); a2.addTopico(t8); a2.addTopico(t19);
        a3.addTopico(t2); a3.addTopico(t4); a3.addTopico(t12); a3.addTopico(t15);
        a4.addTopico(t3); a4.addTopico(t9); a4.addTopico(t16); a4.addTopico(t21);
        a5.addTopico(t13); a5.addTopico(t16); a5.addTopico(t17); a5.addTopico(t21);
        a6.addTopico(t18); a6.addTopico(t5);a6.addTopico(t20); a6.addTopico(t27);
        a7.addTopico(t4); a7.addTopico(t7); a7.addTopico(t12); a7.addTopico(t6);
        a8.addTopico(t18);a8.addTopico(t21);a8.addTopico(t26); a8.addTopico(t27);
        a9.addTopico(t7); a9.addTopico(t17);
         
    }
    
    //Atribuição dos tópicos aos revisores
    public void atribuicaoTopicosAosRevisores(){
        r1.addTopicoDeEspecialidade(t1); r1.addTopicoDeEspecialidade(t2);
        r1.addTopicoDeEspecialidade(t3); r1.addTopicoDeEspecialidade(t4);
        r1.addTopicoDeEspecialidade(t6); r1.addTopicoDeEspecialidade(t7);
        r1.addTopicoDeEspecialidade(t8); r1.addTopicoDeEspecialidade(t10);
        r1.addTopicoDeEspecialidade(t11); r1.addTopicoDeEspecialidade(t12);
        r1.addTopicoDeEspecialidade(t20); r1.addTopicoDeEspecialidade(t1);
        r1.addTopicoDeEspecialidade(t23); r1.addTopicoDeEspecialidade(t27);
        r2.addTopicoDeEspecialidade(t2);  r2.addTopicoDeEspecialidade(t8);
        r2.addTopicoDeEspecialidade(t10); r2.addTopicoDeEspecialidade(t12);
        r2.addTopicoDeEspecialidade(t14); r2.addTopicoDeEspecialidade(t16);
        r2.addTopicoDeEspecialidade(t18); r2.addTopicoDeEspecialidade(t20);
        r2.addTopicoDeEspecialidade(t22); r2.addTopicoDeEspecialidade(t24);
        r2.addTopicoDeEspecialidade(t26); r2.addTopicoDeEspecialidade(t4);
        r2.addTopicoDeEspecialidade(t6);  r3.addTopicoDeEspecialidade(t1);
        r3.addTopicoDeEspecialidade(t2);  r3.addTopicoDeEspecialidade(t3);
        r3.addTopicoDeEspecialidade(t4);  r3.addTopicoDeEspecialidade(t5);
        r3.addTopicoDeEspecialidade(t6);  r3.addTopicoDeEspecialidade(t7);
        r3.addTopicoDeEspecialidade(t8);  r3.addTopicoDeEspecialidade(t9);
        r3.addTopicoDeEspecialidade(t19); r3.addTopicoDeEspecialidade(t20);
        r3.addTopicoDeEspecialidade(t21); r3.addTopicoDeEspecialidade(t22);
        r3.addTopicoDeEspecialidade(t23); r3.addTopicoDeEspecialidade(t24);
        r3.addTopicoDeEspecialidade(t27); r4.addTopicoDeEspecialidade(t10);
        r4.addTopicoDeEspecialidade(t11); r4.addTopicoDeEspecialidade(t12);
        r4.addTopicoDeEspecialidade(t13); r4.addTopicoDeEspecialidade(t14);
        r4.addTopicoDeEspecialidade(t15); r4.addTopicoDeEspecialidade(t16);
        r4.addTopicoDeEspecialidade(t17); r4.addTopicoDeEspecialidade(t18);
        r4.addTopicoDeEspecialidade(t19); r5.addTopicoDeEspecialidade(t1);
        r5.addTopicoDeEspecialidade(t3);  r5.addTopicoDeEspecialidade(t5);
        r5.addTopicoDeEspecialidade(t7);  r5.addTopicoDeEspecialidade(t9);
        r5.addTopicoDeEspecialidade(t11); r5.addTopicoDeEspecialidade(t13);
        r5.addTopicoDeEspecialidade(t15); r5.addTopicoDeEspecialidade(t17);
        r5.addTopicoDeEspecialidade(t19); r5.addTopicoDeEspecialidade(t21);
        r5.addTopicoDeEspecialidade(t23); r5.addTopicoDeEspecialidade(t25);
        r5.addTopicoDeEspecialidade(t27); r6.addTopicoDeEspecialidade(t1);
        r6.addTopicoDeEspecialidade(t2);  r6.addTopicoDeEspecialidade(t3);
        r6.addTopicoDeEspecialidade(t4);  r6.addTopicoDeEspecialidade(t11);
        r6.addTopicoDeEspecialidade(t12); r6.addTopicoDeEspecialidade(t13);
        r6.addTopicoDeEspecialidade(t14); r6.addTopicoDeEspecialidade(t15);
        r6.addTopicoDeEspecialidade(t20); r6.addTopicoDeEspecialidade(t21);
        r6.addTopicoDeEspecialidade(t22); r6.addTopicoDeEspecialidade(t23);
        r6.addTopicoDeEspecialidade(t24); r6.addTopicoDeEspecialidade(t25);
        r6.addTopicoDeEspecialidade(t26); r6.addTopicoDeEspecialidade(t27);
        r7.addTopicoDeEspecialidade(t5);  r7.addTopicoDeEspecialidade(t6);
        r7.addTopicoDeEspecialidade(t7);  r7.addTopicoDeEspecialidade(t8);
        r7.addTopicoDeEspecialidade(t9);  r7.addTopicoDeEspecialidade(t10);
        r7.addTopicoDeEspecialidade(t11); r7.addTopicoDeEspecialidade(t12);
        r7.addTopicoDeEspecialidade(t13); r7.addTopicoDeEspecialidade(t14);
        r7.addTopicoDeEspecialidade(t15); r7.addTopicoDeEspecialidade(t16);
        r7.addTopicoDeEspecialidade(t17); r7.addTopicoDeEspecialidade(t18);
        r7.addTopicoDeEspecialidade(t19); r7.addTopicoDeEspecialidade(t20);
        r7.addTopicoDeEspecialidade(t21); r7.addTopicoDeEspecialidade(t22);
        r7.addTopicoDeEspecialidade(t23); r8.addTopicoDeEspecialidade(t23);
        r8.addTopicoDeEspecialidade(t24); r8.addTopicoDeEspecialidade(t25);
        r8.addTopicoDeEspecialidade(t26); r8.addTopicoDeEspecialidade(t27);
        r9.addTopicoDeEspecialidade(t1);  r9.addTopicoDeEspecialidade(t10);
        r9.addTopicoDeEspecialidade(t11); r9.addTopicoDeEspecialidade(t21);
        r9.addTopicoDeEspecialidade(t5);  r9.addTopicoDeEspecialidade(t17);
    }
    
    public void atribuicaoArtigosAosAutores(){
        //TODO
    }
    
}
