package distrevisores.business;

import java.util.ArrayList;

/**
 *
 * @author José Pedro Moreira
 */
public class Artigo {

    // variáveis de instância
    private String titulo;
    private int idArtigo;
    private ArrayList<Topico> topicos = new ArrayList<>();
    private String resumo;
    
    // variável de classe
    private static int numArtigos = 0;
    private static ArrayList<Artigo> todosArtigos = new ArrayList<>();

    /**
     * Construtor vazio
     */
    public Artigo() {
        this("Sem Titulo");
    }
    
    /**
     * construtor com parâmetros
     * @param titulo 
     */
    public Artigo(String titulo) {
        this.titulo = titulo;
        todosArtigos.add(this);
        idArtigo = ++numArtigos;
    }

    //Gets e Sets
    
    /**
     * método para obter uma lista com todos os artigos
     * @return ArrayList the todosArtigos
     */
    public static ArrayList<Artigo> getTodosArtigos() {
        return todosArtigos;
    }
    
    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @return the titulo
     */
    public String getResumo() {
        return resumo;
    }
    
    /**
     * @return the numArtigos
     */
    public static int getNumArtigos() {
        return numArtigos;
    }

    /**
     * @return the idArtigo
     */
    public int getIdArtigo() {
        return idArtigo;
    }

    /**
     * @return the topicos
     */
    public ArrayList<Topico> getTopicos() {
        return topicos;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @param resumo the resumo to set
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }
    
    /**
     * @param topicos the topicos to set
     */
    public void addTopico(Topico top) {
        //sem tópicos repetidos
        if(!(this.topicos.contains(top))){
            this.topicos.add(top);
        }
    }
}
