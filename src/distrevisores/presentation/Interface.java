package distrevisores.presentation;

import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author José Pedro Moreira
 */
public class Interface extends JFrame {

    private JLabel lblMecanismo1;
    private JLabel lblMecanismo2;
    private JLabel lblIsep;
    private JButton btnMecanismo1, btnMecanismo2;
    private static final int LARGURA =400;
    private static final int ALTURA = 200;

    public Interface() {
        super("Mecanismo de Destribuição");
        this.setSize(Interface.LARGURA, Interface.ALTURA);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //Dimension d = new Dimension(800, 600);
      // this.setPreferredSize(d);
       //this.setSize(d);
       //this.setResizable(false);
        //this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        lblMecanismo1 =  new JLabel("<html>Mecanismo 1 : \n"+
                                    "<P>- Critérios de cumprimento obrigatório:\n" +
                                    "<LI>Atribuição de três revisores a cada artigo;\n" +
                                    "<LI>Todos os revisores atribuídos a um artigo têm afinidade com um ou mais tópicos do artigo.\n");
                                   
        lblMecanismo2 = new JLabel ("<html>Mecanismo 2\n" +
                                    "<P>- Critério obrigatório:\n" +
                                    "<LI>Atribuir no mínimo dois revisores por artigo;\n" +
                                    "<LI>Pelo menos um dos revisores atribuídos a um artigo tem afinidade com um ou mais tópicos do artigo."+
                                    "<P>- Critério opcional:\n" +
                                    "<LI>Distribuição equitativa de artigos pelos revisores. ");
        add( lblMecanismo1 , BorderLayout.NORTH);
        add( lblMecanismo2 , BorderLayout.CENTER);
        
       JLabel lblIsep = new JLabel( new ImageIcon("isep.jpg"),JLabel.CENTER );
       
       
 
       criarPainelTexto1();
       criarPainelBotoes();
       
    }

    public void criarPainelTexto1() {
        
        JPanel p = criarPainelBotoes();
        add(p, BorderLayout.SOUTH);
        
    }

    private JPanel criarPainelBotoes() {
        btnMecanismo1 = new JButton("Escolher Mecanismo de Distribuição 1");
        btnMecanismo2 = new JButton("Escolher Mecanismo de Distribuição 2");
        JPanel p = new JPanel();
        p.add(btnMecanismo1);
        p.add(btnMecanismo2);
        return p;
    }

   

}
