/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package distrevisores.business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author José Pedro Moreira
 */
public class Evento {


    //Variável de Instância
    private String titulo;
    private String descricao;
    private Date dataInicio;
    private Date dataFim;
    private String local;
    private int idEdicao;
    private Organizador organizador;
    
    //Variável de classe
    private static int numeroEdicoes = 0;
    private static ArrayList<Evento> eventos = new ArrayList<>();
    
    //Construtores
    /**
     * Construtor vazio
     */
    public Evento(){
        this("sem título","sem descricao",new Date(),new Date(),"sem local",0);
    }
    
/**
 * Construtor com parametros
 * @param titulo O nome do evento
 * @param descricao A descrição do evento
 * @param dataInicio A data de início do evento
 * @param dataFim A data do fim do evento
 * @param local  O local de realização do evento
 */
    public Evento(String titulo, String descricao, Date dataInicio, Date dataFim, String local,int orgID){
        this.titulo = titulo;
        this.descricao = descricao;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.local = local;
        if (Organizador.getOrganizadorByID(orgID).equals(null)){
            this.organizador=new Organizador();
        }else{
            this.organizador = Organizador.getOrganizadorByID(orgID);   // atribuir organizador através do seu id
        }
        idEdicao = ++numeroEdicoes;
        eventos.add(this);
    }

     /**
     * @return the numeroEdicoes
     */
    public static int getNumeroEdicoes() {
        return numeroEdicoes;
    }
    
    
    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the dataInicio
     */
    public Date getDataInicio() {
        return dataInicio;
    }

    /**
     * @param dataInicio the dataInicio to set
     */
    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * @return the dataFim
     */
    public Date getDataFim() {
        return dataFim;
    }

    /**
     * @param dataFim the dataFim to set
     */
    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the idEdicao
     */
    public int getIdEdicao() {
        return idEdicao;
    }

    /**
     * @param idEdicao the idEdicao to set
     */
    public void setIdEdicao(int idEdicao) {
        this.idEdicao = idEdicao;
    }

    /**
     * @return the organizador
     */
    public Organizador getOrganizador() {
        return organizador;
    }

    /**
     * @param organizador the organizador to set
     */
    public void setOrganizador(Organizador organizador) {
        this.organizador = organizador;
    }

    /**
     * 
     */
    public void novoProcesso(){
    
    }

    /**
     * @return the eventos
     */
    public static ArrayList<Evento> getEventos() {
        return eventos;
    }

    
    
    @Override
    public String toString(){
        return String.format("%s, organizado por %s.", titulo,organizador.getNome());
    }
    
    
}
