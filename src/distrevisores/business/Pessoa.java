/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package distrevisores.business;

/**
 *
 * @author José Pedro Moreira
 */
public class Pessoa {
    //variáveis de instância

    private String nome;
    private String email;

    //Construtores
    
    /**
     * Construtor vazio
     */
    public Pessoa() {
    }
    
    /**
     * Construtor com parâmetros
     * @param nome
     * @param email 
     */
    public Pessoa(String nome, String email) {
        this.nome = nome;
        this.email = email;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
}
