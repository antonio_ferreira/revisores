/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package distrevisores.presentation;

import distrevisores.application.DistribuirRevisoesController;

/**
 *
 * @author José Pedro Moreira
 */
public class DistribuirRevisoesUI {
    
    public enum Mecanismo{ MEC1, MEC2 }
    
    private Mecanismo mec = Mecanismo.MEC1;
    private DistribuirRevisoesController distRevController = new DistribuirRevisoesController();
    
    public void setMecanismo(Mecanismo escolha){
        if(escolha == Mecanismo.MEC1){
            mec = Mecanismo.MEC1;
        }else{
            mec = Mecanismo.MEC2;
        }
    }
    
    
}
