package distrevisores.business;

import java.util.ArrayList;

/**
 *
 * @author José Pedro Moreira
 */
public class Topico {

    
    //Variáveis de instância

    private String tema;
    private String codigoACM;
    //Variável de classe
    private static ArrayList<Topico> listaTopicos = new ArrayList<>();

    //Construtor
    /**
     * Construtor vazio
     */
    public Topico() {
        this("sem tema", "sem código ACM");
    }

    /**
     * Construtor com parâmetros
     *
     * @param tema
     */
    public Topico(String tema, String codACM) {
        this.tema = tema;
        this.codigoACM = codigoACM;
        listaTopicos.add(this);
    }

    /**
     * @return the listaTopicos
     */
    public static ArrayList<Topico> getListaTopicos() {
        return listaTopicos;
    }
    
    /**
     * @return the tema
     */
    public String getTema() {
        return tema;
    }

    /**
     * @return the codigoACM
     */
    public String getCodigoACM() {
        return codigoACM;
    }
    
    
}
