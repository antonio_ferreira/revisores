package distrevisores.business;

import java.util.ArrayList;

/**
 *
 * @author José Pedro Moreira
 */
public class Organizador extends Pessoa {


    //variáveis de instância
    private int orgID;
    
    //variáveis de classe
    private static int numOrganizadores = 0;
    private static ArrayList<Organizador> organizadores= new ArrayList<>();
    
    //construtores
    
    /**
     * Construtor vazio
     */
    public Organizador(){
        this("sem nome","sem email");
    }
    
    /**
     * Construtor com parâmetros
     * @param nome
     * @param email 
     */
    public Organizador(String nome, String email){
        super(nome, email);
        orgID = ++numOrganizadores;
        organizadores.add(this);
    }

    /**
     * @return the orgID
     */
    public int getOrgID() {
        return orgID;
    }

    /**
     * @return the numOrganizadores
     */
    public int getNumOrganizadores() {
        return numOrganizadores;
    }
    
    
    //método de classe

    /**
     * @return Organizadores ArrayList
     */
    public static ArrayList<Organizador> getOrganizadores() {
        return organizadores;
    }
      
    /**
     * método para obter o objeto Organizador a partir do seu ID
     * @param orgID
     * @return 
     */
    public static Organizador getOrganizadorByID(int orgID){
        for(Organizador org : organizadores){
            if (org.getOrgID()==orgID)
                return org;
        }
        return null;
    }
    
    
}
