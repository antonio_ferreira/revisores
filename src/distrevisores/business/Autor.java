
package distrevisores.business;

import java.util.ArrayList;

/**
 *
 * @author José Pedro Moreira
 */
public class Autor extends Pessoa{

    //variáveis de instância

    private int idAutor;
    private ArrayList<Artigo> artigos = new ArrayList<>();
    
    //variáveis de Classe
    private static int numAutores = 0; 
    
    //Construtores
    
    public Autor(){
        this("sem nome", "sem email");
    }
    
    public Autor(String nome, String email){
        super(nome,email);
        idAutor = ++numAutores;
    }

    //gets e sets
    
    /**
     * @return the numAutores
     */
    public static int getNumAutores() {
        return numAutores;
    }
    /**
     * @return the idAutor
     */
    public int getIdAutor() {
        return idAutor;
    }

    /**
     * @param idAutor the idAutor to set
     */
    public void setIdAutor(int idAutor) {
        this.idAutor = idAutor;
    }

    /**
     * @return the artigos
     */
    public ArrayList<Artigo> getArtigos() {
        return artigos;
    }

    /**
     * @param artigos the artigos to set
     */
    public void addArtigo(Artigo a) {
        //sem artigos repetidos
        if(!(this.artigos.contains(a))){
            this.artigos.add(a);
        }
    }
    
    
}
