
package distrevisores.business;

/**
 *
 * @author José Pedro Moreira
 */
public class MecanismoDistribuicao2 implements MecanismoDistribuicao{

    @Override
    public void distribui(Evento e) {
        System.out.println("Foi escolhido o mecanismo de distribuição 2");
    }

}
