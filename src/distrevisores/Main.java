
package distrevisores;

import distrevisores.business.Artigo;
import distrevisores.business.Evento;
import distrevisores.business.MecanismoDistribuicao1;
import distrevisores.business.Organizador;
import distrevisores.business.Revisor;
import distrevisores.business.Topico;

/**
 *
 * @author José Pedro Moreira
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{

        final BootStrap boot = new BootStrap();
        boot.atribuicaoTopicosAosArtigos();
        boot.atribuicaoTopicosAosRevisores();
        
        MecanismoDistribuicao1 md;
        
        //imprimir eventos
        System.out.println("\nLista de Eventos\n");
        for (Evento ev: Evento.getEventos()){
            System.out.printf("%s\n",ev.toString());
        }
        
        //imprimir organizadores
        System.out.println("\nLista de Organizadores\n");
        for (Organizador org: Organizador.getOrganizadores()){
            System.out.printf("%s\n", org.getNome());
        }
        
        
        
        //imprimir topicos de especialidade de cada revisor 
        System.out.println("\nLista de tópicos associados a cada revisor\n");
        for (Revisor a: Revisor.getTodosRevisores()){
            System.out.printf("\n%s\n",a.getNome());
            for(Topico t:a.getEspecialistaTopicos())
                System.out.println(t.getTema());
        }
        
        //imprimir tópicos associados a cada artigo
        System.out.println("\nLista de tópicos associados a cada artigo\n");
        for (Artigo a:Artigo.getTodosArtigos()){
            System.out.printf("\n%s\n",a.getTitulo());
            for(Topico t:a.getTopicos())
                System.out.println(t.getTema());
        }
    }
}
