package distrevisores.business;

import distrevisores.business.Artigo;
import java.util.ArrayList;

/**
 *
 * @author José Pedro Moreira
 */
public class Revisor extends Pessoa {

    //variáveis de instância
    private int idRevisor;
    private ArrayList<Topico> especialistaTopicos = new ArrayList<>();
    private ArrayList<Artigo> artigosAtribuidos = new ArrayList<>();
    
    //variáveis de Classe
    private static int numRevisores = 0;
    private static ArrayList<Revisor> todosRevisores= new ArrayList<>();

    //Construtores
    /**
     * Construtor vazio
     */
    public Revisor() {
        this("sem nome", "sem email");
        }
    
    /**
     * Construtor com parâmetros
     * @param nome
     * @param email 
     */
    public Revisor(String nome, String email){
        super(nome, email);
        idRevisor = ++numRevisores;
        todosRevisores.add(this);
    }
            
    // Gets e sets

    /**
     * @return the numRevisores
     */
    public static int getNumRevisores() {
        return numRevisores;
    }
    
    /**
     * @return the idRevisor
     */
    public int getIdRevisor() {
        return idRevisor;
    }

    /**
     * @return the especialistaTopicos
     */
    public ArrayList<Topico> getEspecialistaTopicos() {
        return especialistaTopicos;
    }

    /**
     * @param especialistaTopicos the especialistaTopicos to set
     */
    public void addTopicoDeEspecialidade(Topico t) {
        if(!(this.especialistaTopicos.contains(t))){
            this.especialistaTopicos.add(t);
        }
    }
    
    /**
     * Método para ter acesso à lista com todos os revisores
     * @return the todosRevisores
     */
    public static ArrayList<Revisor> getTodosRevisores(){
        return todosRevisores;
    }
    
    /**
     * Método para ter acesso à lista com todos os revisores
     * @return the artigosAtribuidos
     */
    public ArrayList<Artigo> getArtigosAtribuidos(){
        return artigosAtribuidos;
    }
    
}
